# iMYNcoin App for managing [Jitterbugs](https://gitlab.com/Crypteriat/jitterbug)
## Based from:  https://mikebars.github.io/universal-react-native-boilerplate/
Universal React Native Boilerplate - a bare bones combination of [create-react-app](https://github.com/facebookincubator/create-react-app), [react-native](https://github.com/facebook/react-native), and [react-native-web](https://github.com/necolas/react-native-web)

A simple boilerplate to write one app that runs on web, iOS, and Android!

### Quick Start
```
# checkout repo
git clone git@gitlab.com:Crypteriat/WebApps/mynapp.git
cd mynapp
# use nvm for automatic NodeJS switching, see: https://github.com/creationix/nvm
nvm install `cat mynapp/.nvmrc` 
npm i -g yarn
```

```
## install dependencies
yarn
```

Available commands:

```
# run web project in dev server
yarn start-web
```
```
# start ios project in emulator
yarn start-ios
```
```
# start android project in (already running) emulator
yarn start-android
```
```
# build web project for production
yarn build
```
```
# test application
yarn test
```
