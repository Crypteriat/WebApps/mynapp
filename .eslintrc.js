module.exports = {
  'env': {
    'jest': true,
    'browser': true,
    'es6': true,
    'node': true
  },
  'extends': [
    'airbnb',
    'plugin:flowtype/recommended'
  ],
  'parserOptions': {
    'ecmaFeatures': {
        'experimentalObjectRestSpread': true,
        'jsx': true
      },
      'sourceType': 'module'
    },
  'plugins': [
    'import',
    'flowtype',
    'filenames-suffix',
    'react',
  ],
  'globals': {
    'window': true
  },
  'settings': {
    'import/ignore': [
      'node_modules',
      '\\.json$',
      '\\.(scss|less|css)$'
    ],
    'import/resolve': {
      'extensions': [
        '.js',
        '.jsx',
        '.json'
      ]
    }
  },
  'rules': {
    // filenames-suffix
    // NOTE:  making this a warning first, then it will move to an error
    'filenames-suffix/match-regex': 0,  // no need for a regex match
    'filenames-suffix/match-exported': 0,  // filenames must match default export
    'filenames-suffix/no-index': 0,  // we like index.js
    // Soften some rules.
    'react/prefer-stateless-function': 0,
    "jsx-a11y/anchor-has-content": 0,
    'react/jsx-filename-extension': [1, { 'extensions': ['.js', '.jsx'] }],
    'react/jsx-boolean-value': 0, // throwing an incsonsistency err between airbnb/react
    'react/no-unused-prop-types': [0],
    'react/no-array-index-key': [0],
    'no-unused-vars': [2, { 'args': 'none' }],
    'quote-props': 0,
    'comma-dangle': 0, // Nobody cares about commas.
    'no-plusplus': 0, // removing for now
    'no-template-curly-in-string': 0, // removing for now
    'no-mixed-operators': 0, // removing for now
    'no-restricted-syntax': 0, // removing for now
    'default-case': 0, // Required default case is nonsense.
    'new-cap': [2, {'capIsNew': false, 'newIsCap': true}], // For Record() etc.
    'no-floating-decimal': 0, // .5 is just fine.
    'no-param-reassign': 0, // We love param reassignment.
    'no-class-assign': 0, // We also love to reassign class names.
    'no-shadow': 0, // Shadowing is a nice language feature.
    // eslint-plugin-import
    'import/no-unresolved': [2, {'commonjs': true}],
    'import/named': 2,
    'import/default': 2,
    'import/namespace': 2,
    'import/export': 2,
    'import/no-named-as-default': 0,
    'radix': 0,
    'no-confusing-arrow': ['error', {'allowParens': true}],
    'max-len': [1, 100, 2, {'ignoreStrings': true, 'ignoreTemplateLiterals':true, 'ignoreUrls':true, 'ignoreTrailingComments':true}],
    'global-require': 0,
    'no-console': [1, { 'allow': ['warn', 'error'] }],
    'no-use-before-define': ['error', { 'functions': false, 'classes': true }],
     'import/no-named-default': 0, 
    'jsx-a11y/href-no-hash': 0,
    // 'no-mixed-operators': 0,
    // 'class-methods-use-this': 0
  },
};
